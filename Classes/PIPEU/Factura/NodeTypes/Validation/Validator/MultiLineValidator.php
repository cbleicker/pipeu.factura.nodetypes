<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\NodeTypes\Validation\Validator;

use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Validation\Validator\AbstractValidator;
use TYPO3\Flow\Validation\Exception\InvalidValidationOptionsException;

/**
 * Class MultiLineValidator
 *
 * @package PIPEU\Factura\NodeTypes\Validation\Validator
 */
class MultiLineValidator extends AbstractValidator {

	/**
	 * Also validate empty values (NULL)
	 *
	 * @var boolean
	 */
	protected $acceptsEmptyValues = TRUE;

	/**
	 * @var array
	 */
	protected $supportedOptions = array(
		'minimum' => array(0, 'Minimum number of lines for a valid string', 'integer'),
		'maximum' => array(PHP_INT_MAX, 'Maximum number of lines for a valid string', 'integer')
	);

	/**
	 * @param mixed $value
	 *
	 * @throws InvalidValidationOptionsException
	 * @return void
	 */
	protected function isValid($value) {

		if($value === NULL){
			$value = '';
		}

		if (!is_string($value)) {
			throw new InvalidValidationOptionsException('$value is not of type string. ' . gettype($value) . ' given.', 1397734699);
		}

		if ($this->options['maximum'] < $this->options['minimum']) {
			throw new InvalidValidationOptionsException('The \'maximum\' is less than the \'minimum\' in the MultiLineValidator.', 1397734700);
		}

		$count = substr_count($value, "\n");

		$isValid = TRUE;

		if ($count < $this->options['minimum'] - 1) {
			$isValid = FALSE;
		}
		if ($count > $this->options['maximum'] - 1) {
			$isValid = FALSE;
		}

		if ($isValid === FALSE) {
			if ($this->options['minimum'] > 0 && $this->options['maximum'] < PHP_INT_MAX) {
				$this->addError('The number of newlines must be between %1$d and %2$d.', 1397734701, array($this->options['minimum'], $this->options['maximum']));
			} elseif ($this->options['minimum'] > 0) {
				$this->addError('More than %1$d newlines expected.', 1397734702, array($this->options['minimum']));
			} else {
				$this->addError('Less than %1$d newlines expected.', 1397734703, array($this->options['maximum']));
			}
		}
	}
}
