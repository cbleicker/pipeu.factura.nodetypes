<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\NodeTypes\Validation\Validator\Dto;

use PIPEU\Factura\NodeTypes\Validation\Validator\MultiLineValidator;
use PIPEU\Factura\Validation\Validator\NonViesVatValidator;
use PIPEU\Factura\Validation\Validator\ViesVatValidator;
use TYPO3\Flow\Reflection\ObjectAccess;
use TYPO3\Flow\Validation\Validator\AbstractValidator;
use TYPO3\Flow\Validation\Validator\EmailAddressValidator;
use TYPO3\Flow\Validation\Validator\NotEmptyValidator;
use TYPO3\Flow\Validation\Validator\StringLengthValidator;
use TYPO3\Flow\Validation\ValidatorResolver;
use TYPO3\Flow\Reflection\ReflectionService;
use TYPO3\Flow\Annotations as Flow;
use PIPEU\Factura\NodeTypes\Domain\Model\InvoiceDto;

/**
 * Class InvoiceDtoValidator
 *
 * @package PIPEU\Factura\NodeTypes\Validation\Validator\Dto
 */
class InvoiceDtoValidator extends AbstractValidator {

	/**
	 * @var ReflectionService
	 * @Flow\Inject
	 */
	protected $reflectionService;

	/**
	 * @var ValidatorResolver
	 * @Flow\Inject
	 */
	protected $validationResolver;

	/**
	 * @param InvoiceDto $value
	 * @return void
	 */
	protected function isValid($value) {
		$invoiceDto = $value;
		$notEmptyValidator = new NotEmptyValidator();
		$emailAddressValidator = new EmailAddressValidator();
		$multilineValidator = new MultiLineValidator([
			'minimum' => 1,
			'maximum' => 2
		]);

		$stringLengthValidator = new StringLengthValidator([
			'minimum' => 0,
			'maximum' => 255
		]);

		$postalCodeLengthValidator = new StringLengthValidator([
			'minimum' => 0,
			'maximum' => 20
		]);

		$iso2 = ObjectAccess::getPropertyPath($invoiceDto, 'postal.country.iso2');

		if ($iso2 !== NULL) {
			$viesVatValidator = new ViesVatValidator(array('webservice' => FALSE, 'countryIso2' => $invoiceDto->getPostal()->getCountry()->getIso2()));
			$nonViesVatValidator = new NonViesVatValidator(array('webservice' => FALSE, 'countryIso2' => $invoiceDto->getPostal()->getCountry()->getIso2()));

			if ($invoiceDto->getPostal()->getCountry()->getEuMember()) {
				$propertyPath = 'vat';
				$this->result->forProperty($propertyPath)->merge($viesVatValidator->validate(ObjectAccess::getPropertyPath($invoiceDto, $propertyPath)));
			}

			if (!$invoiceDto->getPostal()->getCountry()->getEuMember()) {
				$propertyPath = 'vat';
				$this->result->forProperty($propertyPath)->merge($nonViesVatValidator->validate(ObjectAccess::getPropertyPath($invoiceDto, $propertyPath)));
			}
		}

		$propertyPath = 'personName.title';
		$this->result->forProperty($propertyPath)->merge($stringLengthValidator->validate(ObjectAccess::getPropertyPath($invoiceDto, $propertyPath)));

		$propertyPath = 'company';
		$this->result->forProperty($propertyPath)->merge($stringLengthValidator->validate(ObjectAccess::getPropertyPath($invoiceDto, $propertyPath)));

		$propertyPath = 'personName.firstName';
		$this->result->forProperty($propertyPath)->merge($notEmptyValidator->validate(ObjectAccess::getPropertyPath($invoiceDto, $propertyPath)));
		$this->result->forProperty($propertyPath)->merge($stringLengthValidator->validate(ObjectAccess::getPropertyPath($invoiceDto, $propertyPath)));

		$propertyPath = 'personName.lastName';
		$this->result->forProperty($propertyPath)->merge($notEmptyValidator->validate(ObjectAccess::getPropertyPath($invoiceDto, $propertyPath)));
		$this->result->forProperty($propertyPath)->merge($stringLengthValidator->validate(ObjectAccess::getPropertyPath($invoiceDto, $propertyPath)));

		$propertyPath = 'postal.address';
		$this->result->forProperty($propertyPath)->merge($notEmptyValidator->validate(ObjectAccess::getPropertyPath($invoiceDto, $propertyPath)));
		$this->result->forProperty($propertyPath)->merge($multilineValidator->validate(ObjectAccess::getPropertyPath($invoiceDto, $propertyPath)));
		$this->result->forProperty($propertyPath)->merge($stringLengthValidator->validate(ObjectAccess::getPropertyPath($invoiceDto, $propertyPath)));

		$propertyPath = 'postal.code';
		$this->result->forProperty($propertyPath)->merge($notEmptyValidator->validate(ObjectAccess::getPropertyPath($invoiceDto, $propertyPath)));
		$this->result->forProperty($propertyPath)->merge($postalCodeLengthValidator->validate(ObjectAccess::getPropertyPath($invoiceDto, $propertyPath)));

		$propertyPath = 'postal.city';
		$this->result->forProperty($propertyPath)->merge($notEmptyValidator->validate(ObjectAccess::getPropertyPath($invoiceDto, $propertyPath)));
		$this->result->forProperty($propertyPath)->merge($stringLengthValidator->validate(ObjectAccess::getPropertyPath($invoiceDto, $propertyPath)));

		$propertyPath = 'postal.country';
		$this->result->forProperty($propertyPath)->merge($notEmptyValidator->validate(ObjectAccess::getPropertyPath($invoiceDto, $propertyPath)));

		$propertyPath = 'postal.county';
		$this->result->forProperty($propertyPath)->merge($stringLengthValidator->validate(ObjectAccess::getPropertyPath($invoiceDto, $propertyPath)));

		$propertyPath = 'electronicAddress.identifier';
		$this->result->forProperty($propertyPath)->merge($emailAddressValidator->validate(ObjectAccess::getPropertyPath($invoiceDto, $propertyPath)));
	}
}
