<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\NodeTypes\Aop;

use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Aop\JoinPointInterface;
use TYPO3\Flow\Error\Exception;
use TYPO3\Flow\Log\SystemLoggerInterface;
use Doctrine\DBAL\DBALException;

/**
 * Class MediaAspect
 *
 * @package Wysiwyg\Vamos\Aop
 * @Flow\Aspect
 */
class PersistenceManagerAspect {

	/**
	 * @Flow\Inject
	 * @var SystemLoggerInterface
	 */
	protected $systemLogger;

	/**
	 * If a user submitting the order with a double-click we ran into a unique constraint exception
	 * We need to handle this and keep the application working as expected.
	 * @Flow\Around("method(TYPO3\Flow\Persistence\Doctrine\PersistenceManager->persistAll())")
	 *
	 * @param JoinPointInterface $joinPoint The current join point
	 * @return mixed
	 * @throws DBALException
	 */
	public function catchPdoExceptionForDoubleClicks(JoinPointInterface $joinPoint) {
		try {
			return $joinPoint->getAdviceChain()->proceed($joinPoint);
		} catch (DBALException $e) {
			if (preg_match('/Integrity constraint violation/', $e->getMessage())) {
				$exception = new Exception('Double click detected', 1413529592, $e);
				$this->systemLogger->logException($exception);
			} else {
				throw $e;
			}
		}
	}
}
