<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\NodeTypes\Aspect;

use PIPEU\Factura\Domain\Abstracts\AbstractFacturaItem;
use TYPO3\Flow\Reflection\ObjectAccess;
use TYPO3\TYPO3CR\Domain\Model\NodeInterface as NeosNodeInterface;
use TYPO3\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class AbstractFacturaItemAspect
 *
 * @package PIPEU\Factura\NodeTypes\Aspect
 * @Flow\Scope("singleton")
 * @Flow\Aspect
 * @Flow\Introduce("class(PIPEU\Factura\Domain\Abstracts\AbstractFacturaItem)", interfaceName="PIPEU\Factura\NodeTypes\Aspect\ContextPathInterface")
 */
class AbstractFacturaItemAspect {

	/**
	 * @var string
	 * @Flow\Transient
	 * @Flow\Introduce("class(PIPEU\Factura\Domain\Abstracts\AbstractFacturaItem)")
	 */
	protected $contextPath;

	/**
	 * @param  \TYPO3\Flow\AOP\JoinPointInterface $joinPoint The current join point
	 * @return mixed
	 * @Flow\Around("method(PIPEU\Factura\Domain\Abstracts\AbstractFacturaItem->setContextPath())")
	 */
	public function setContextPathImplementation(\TYPO3\Flow\AOP\JoinPointInterface $joinPoint) {
		$joinPoint->getAdviceChain()->proceed($joinPoint);
		/** @var AbstractFacturaItem $proxy */
		$proxy = $joinPoint->getProxy();
		/** @var string $contextPath */
		$contextPath = $joinPoint->getMethodArgument('contextPath');
		ObjectAccess::setProperty($proxy, 'contextPath', $contextPath, TRUE);
		return $proxy;
	}

	/**
	 * @param  \TYPO3\Flow\AOP\JoinPointInterface $joinPoint The current join point
	 * @return string
	 * @Flow\Around("method(PIPEU\Factura\Domain\Abstracts\AbstractFacturaItem->getContextPath())")
	 */
	public function getNodeImplementation(\TYPO3\Flow\AOP\JoinPointInterface $joinPoint) {
		/** @var AbstractFacturaItem $proxy */
		$proxy = $joinPoint->getProxy();
		return ObjectAccess::getProperty($proxy, 'contextPath', TRUE);
	}
}
