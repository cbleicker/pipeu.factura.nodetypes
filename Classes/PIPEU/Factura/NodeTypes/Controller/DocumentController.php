<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\NodeTypes\Controller;

use PIPEU\Factura\Domain\Abstracts\AbstractDocument;
use PIPEU\Factura\Domain\Interfaces\InterfaceDocument;
use PIPEU\Factura\Domain\Interfaces\InterfaceState;
use PIPEU\Factura\Domain\Model\State;
use PIPEU\Factura\NodeTypes\Controller\Abstracts\AbstractPluginController;
use PIPEU\Payment\Domain\Model\Interfaces\PaymentTypeInterface;
use TYPO3\Flow\Property\PropertyMapper;
use TYPO3\Flow\Error\Message;
use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Mvc\Controller\ControllerContext;
use TYPO3\Flow\Resource\Resource as FileResource;
use TYPO3\Flow\Security\Exception\AccessDeniedException;
use TYPO3\Flow\Utility\MediaTypes;
use PIPEU\Factura\Domain\Model\Documents\Invoice;
use PIPEU\Factura\Security\Utility as SecurityUtility;
use TYPO3\Jobqueue\Common\Job\JobManager;
use PIPEU\Payment\Domain\Service\Interfaces\PaymentTypeServiceInterface;

/**
 * Class DocumentController
 *
 * @package PIPEU\Factura\NodeTypes\Controller
 */
class DocumentController extends AbstractPluginController {

	/**
	 * @var JobManager
	 * @Flow\Inject
	 */
	protected $jobManager;

	/**
	 * @var PropertyMapper
	 * @Flow\Inject
	 */
	protected $propertyMapper;

	/**
	 * @var PaymentTypeServiceInterface
	 * @Flow\Inject
	 */
	protected $paymentService;

	/**
	 * @throws AccessDeniedException
	 * @return void
	 */
	protected function initializeShowAction() {
		$arguments = $this->request->getArguments();
		if (!SecurityUtility::validateSaltedMd5($arguments, 'hash', 'document.__identity')) {
			throw new AccessDeniedException("Access denied", 1401652980);
		}
	}

	/**
	 * @param AbstractDocument $document
	 * @return void
	 */
	public function showAction(AbstractDocument $document) {
		if($document instanceof Invoice){
			$paymentTypes = $this->paymentService->getSupportedPaymentTypes($document);
			$this->view->assign('paymentTypes', $paymentTypes);
		}
		$this->view->assign('document', $document);
	}

	/**
	 * @throws AccessDeniedException
	 * @return void
	 */
	protected function initializeApproveAction() {
		$arguments = $this->request->getArguments();
		if (SecurityUtility::validateSaltedMd5($arguments, 'hash', 'document.__identity', 'email')) {
			throw new AccessDeniedException("Access denied", 1401652981);
		}
	}

	/**
	 * @param AbstractDocument $document
	 * @return void
	 */
	public function approveAction(AbstractDocument $document) {
		if (!$document->getPrimaryElectronicAddress()->isApproved()) {
			$state = new State(InterfaceState::CODE_APPROVED, InterfaceState::SEVERITY_OK, 'Approved');
			$document->setPrimaryState($state)->getPrimaryElectronicAddress()->setApproved(TRUE);
			$this->persistenceManager->update($document);
			$this->persistenceManager->persistAll();
			$this->addFlashMessage('Approvement was successful', 'Approvement', NULL, array(), 1401642664);
			$sourceType = $this->reflectionService->getClassNameByObject($document);
			if ($sourceType === 'PIPEU\Factura\Domain\Model\Documents\Order') {
				$targetType = 'PIPEU\Factura\Domain\Model\Documents\Invoice';
				$invoice = $document->getChildDocuments()->filter(AbstractDocument::childTypeFilter($targetType))->first();
				if (!($invoice instanceof Invoice)) {
					/** @var Invoice $invoice */
					$invoice = $this->propertyMapper->convert($document, $targetType);
					$this->persistenceManager->add($invoice);
					$this->persistenceManager->persistAll();
					$this->emitInvoiceCreated($invoice, $this->controllerContext);
				}
				$this->redirect('show', NULL, NULL, array('document' => $invoice, 'hash' => $invoice->getIdentityHash()));
			}
		} else {
			$this->addFlashMessage('Approvement already done', 'Approvement', Message::SEVERITY_NOTICE, array(), 1401642665);
		}

		$this->redirect('show', NULL, NULL, array('document' => $document, 'hash' => $document->getIdentityHash()));
	}

	/**
	 * @param AbstractDocument $document
	 * @param ControllerContext $controllerContext
	 * @return void
	 * @Flow\Signal
	 */
	protected function emitInvoiceCreated(AbstractDocument $document, ControllerContext $controllerContext) {
	}

	/**
	 * @throws AccessDeniedException
	 * @return void
	 */
	protected function initializeDownloadAction() {
		$arguments = $this->request->getArguments();
		if (!SecurityUtility::validateSaltedMd5($arguments, 'hash', 'document.__identity')) {
			throw new AccessDeniedException("Access denied", 1401652982);
		}
	}

	/**
	 * @param AbstractDocument $document
	 * @return void
	 */
	public function downloadAction(AbstractDocument $document) {
		$resource = $this->findResource($document);
		if ($resource instanceof FileResource) {
			$this->response->setHeader('Content-type', MediaTypes::getMediaTypeFromFilename($resource->getFilename()));
			$this->response->setHeader('Content-disposition', 'attachment; filename="' . $document->getSerialNumber() . '.' . $resource->getFileExtension() . '"');
			$this->response->setContent(file_get_contents($resource->getUri()));
			$this->response->send();
		} else {
			$this->emitMissingDownload($document, $this->controllerContext);
			$this->forward('queued', NULL, NULL, array('document' => $document, 'hash' => $document->getIdentityHash()));
		}
	}

	/**
	 * @param AbstractDocument $document
	 * @param ControllerContext $controllerContext
	 * @return void
	 * @Flow\Signal
	 */
	protected function emitMissingDownload(AbstractDocument $document, ControllerContext $controllerContext) {
	}

	/**
	 * @throws AccessDeniedException
	 * @return void
	 */
	protected function initializeQueuedAction() {
		$arguments = $this->request->getArguments();
		if (!SecurityUtility::validateSaltedMd5($arguments, 'hash', 'document.__identity')) {
			throw new AccessDeniedException("Access denied", 1401652983);
		}
	}

	/**
	 * @param AbstractDocument $document
	 * @return void
	 */
	public function queuedAction(AbstractDocument $document) {
		$this->addFlashMessage('Document is queued', 'Queued', Message::SEVERITY_NOTICE, array(), 1401520368);
		$this->redirect('show', NULL, NULL, array('document' => $document, 'hash' => $document->getIdentityHash()));
	}

	/**
	 * @param InterfaceDocument $source
	 * @return string
	 */
	protected function generateFileName(InterfaceDocument $source) {
		return $source->getType() . '.' . $this->persistenceManager->getIdentifierByObject($source) . '.pdf';
	}

	/**
	 * @param InterfaceDocument $source
	 * @return FileResource
	 */
	protected function findResource(InterfaceDocument $source) {
		$resourceType = 'TYPO3\Flow\Resource\Resource';
		$query = $this->persistenceManager->createQueryForType($resourceType);
		$query->matching(
			$query->equals('filename', $this->generateFileName($source))
		);
		return $query->execute()->getFirst();
	}
}
