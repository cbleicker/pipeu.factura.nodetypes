<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\NodeTypes\Controller\Abstracts;

use TYPO3\Flow\Mvc\Controller\ActionController;
use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Error\Error;
use TYPO3\Flow\I18n\Service as I18nService;

/**
 * Class AbstractPluginController
 *
 * @package PIPEU\Factura\NodeTypes\Controller\Abstracts
 */
abstract class AbstractPluginController extends ActionController {

	/**
	 * @var I18nService
	 * @Flow\Inject
	 */
	protected $i18nService;

	/**
	 * @return Error
	 */
	protected function getErrorFlashMessage() {
		if($this->arguments->getValidationResults()->hasErrors()){
			return new Error('Validation failed.', 1411390396);
		}
		return parent::getErrorFlashMessage();
	}
}
