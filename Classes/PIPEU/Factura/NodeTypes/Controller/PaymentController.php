<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\NodeTypes\Controller;

use PIPEU\Factura\Domain\Model\Documents\Invoice;
use PIPEU\Factura\NodeTypes\Controller\Abstracts\AbstractPluginController;
use PIPEU\Payment\Domain\Model\Abstracts\AbstractCreditCard;
use PIPEU\Payment\Domain\Model\EpsTransfer;
use PIPEU\Payment\Domain\Model\GiroPayTransfer;
use PIPEU\Payment\Domain\Model\IDealTransfer;
use PIPEU\Payment\Domain\Model\InstantTransfer;
use PIPEU\Payment\Domain\Service\Exceptions\MaximumAmountException;
use PIPEU\Payment\Domain\Service\Exceptions\PaymentSuperTypeNotSupported;
use PIPEU\Payment\Domain\Service\Exceptions\PaymentTypeNotSupportedException;
use PIPEU\PayOne\Http\ApiResponse;
use PIPEU\PayOne\Http\ApiResult;
use PIPEU\Payment\Domain\Model\Abstracts\AbstractPaymentType;
use PIPEU\Payment\Domain\Service\Exceptions\CountryNotSupportedException;
use PIPEU\Payment\Domain\Service\Exceptions\MinimumAmountException;
use PIPEU\PayOne\Domain\Model\Transaction;
use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Error\Message;
use TYPO3\Flow\Property\TypeConverter\ObjectConverter;
use PIPEU\Factura\Security\Utility as SecurityUtility;
use TYPO3\Flow\Security\Exception\AccessDeniedException;
use PIPEU\Payment\Domain\Service\Interfaces\TransmitterInterface as PaymentTransmitterInterface;
use PIPEU\PayOne\Log\Interfaces\ApiLoggerInterface;

/**
 * Class PaymentController
 *
 * @package PIPEU\Factura\NodeTypes\Controller
 */
class PaymentController extends AbstractPluginController {

	/**
	 * @var PaymentTransmitterInterface
	 * @Flow\Inject
	 */
	protected $paymentTransmitter;

	/**
	 * @var ApiLoggerInterface
	 * @Flow\Inject
	 */
	protected $apiLogger;

	/**
	 * @throws AccessDeniedException
	 * @return void
	 */
	protected function initializeNewAction() {
		$paymentTypeMappingConfiguration = $this->arguments->getArgument('paymentType')->getPropertyMappingConfiguration();
		$paymentTypeMappingConfiguration->setTypeConverterOption('TYPO3\Flow\Property\TypeConverter\ObjectConverter', ObjectConverter::CONFIGURATION_OVERRIDE_TARGET_TYPE_ALLOWED, TRUE);
		$arguments = $this->request->getArguments();
		if (!SecurityUtility::validateSaltedMd5($arguments, 'hash', 'document.__identity')) {
			throw new AccessDeniedException("Access denied", 1414682434);
		}
	}

	/**
	 * @param AbstractPaymentType $paymentType
	 * @param Invoice $document
	 * @return void
	 * @throws PaymentSuperTypeNotSupported
	 */
	public function newAction(AbstractPaymentType $paymentType, Invoice $document) {
		try {
			if ($this->paymentTransmitter->canHandle($paymentType, $document)) {
				$this->view->assign('paymentType', $paymentType);
				$this->view->assign('document', $document);
				switch ($paymentType->getType()) {
					case 'PIPEU\Payment\Domain\Model\MasterCard':
					case 'PIPEU\Payment\Domain\Model\VisaCard':
						$this->creditCardAssignments($paymentType, $document);
						break;
					case 'PIPEU\Payment\Domain\Model\GiroPayTransfer':
					case 'PIPEU\Payment\Domain\Model\InstantTransfer':
						$this->transferAssignments($paymentType, $document);
						break;
					case 'PIPEU\Payment\Domain\Model\IDealTransfer':
						$this->iDealTransferAssignments($paymentType, $document);
						break;
					case 'PIPEU\Payment\Domain\Model\EpsTransfer':
						$this->epsTransferAssignments($paymentType, $document);
						break;
					default:
						throw new PaymentSuperTypeNotSupported('This paymentSuperType is not supported', 1414519331);
						break;
				}
			}
		} catch (CountryNotSupportedException $exception) {
			$this->addFlashMessage('We are sorry, the selected type of payment is not supported your country.', Message::SEVERITY_ERROR, Message::SEVERITY_ERROR, array(), 1414836779);
			$this->redirect('show', 'Document', NULL, array('document' => $document, 'hash' => $document->getIdentityHash()));
		} catch (MinimumAmountException $exception) {
			$this->addFlashMessage('We are sorry, the selected type of payment is not supported for such a less amount.', Message::SEVERITY_ERROR, Message::SEVERITY_ERROR, array(), 1414836780);
			$this->redirect('show', 'Document', NULL, array('document' => $document, 'hash' => $document->getIdentityHash()));
		} catch (MaximumAmountException $exception) {
			$this->addFlashMessage('We are sorry, the selected type of payment is not supported for such a big amount.', Message::SEVERITY_ERROR, Message::SEVERITY_ERROR, array(), 1414836781);
			$this->redirect('show', 'Document', NULL, array('document' => $document, 'hash' => $document->getIdentityHash()));
		} catch (PaymentTypeNotSupportedException $exception) {
			$this->addFlashMessage('We are sorry, the selected type of payment is not supported in general.', Message::SEVERITY_ERROR, Message::SEVERITY_ERROR, array(), 1414836782);
			$this->redirect('show', 'Document', NULL, array('document' => $document, 'hash' => $document->getIdentityHash()));
		} catch (\Exception $exception) {
			$this->apiLogger->logException($exception);
			$this->addFlashMessage('We are sorry, there are some technical problems we need to fix.', Message::SEVERITY_ERROR, Message::SEVERITY_ERROR, array(), 1414836783);
			$this->redirect('show', 'Document', NULL, array('document' => $document, 'hash' => $document->getIdentityHash()));
		}
	}

	/**
	 * @param AbstractPaymentType $paymentType
	 * @param Invoice $document
	 * @return void
	 */
	protected function creditCardAssignments(AbstractPaymentType $paymentType, Invoice $document) {
		$this->request->getMainRequest()->setArguments(array('node' => $this->request->getMainRequest()->getArgument('node')));
		$transaction = Transaction::create($paymentType);
		$transaction->offsetSet('request', 'creditcardcheck')
			->offsetSet('responsetype', 'REDIRECT')
			->offsetSet('storecarddata', 'yes')
			->offsetSet('encoding', 'UTF-8')
			->offsetSet('successurl', $this->uriBuilder->reset()->setCreateAbsoluteUri(TRUE)->uriFor('creditCardAuthorization', array('paymentType' => array('__type' => $paymentType->getType()), 'document' => $document, 'hash' => $document->getIdentityHash())))
			->offsetSet('errorurl', $this->uriBuilder->reset()->setCreateAbsoluteUri(TRUE)->uriFor('paymentError', array('paymentType' => array('__type' => $paymentType->getType()), 'document' => $document, 'hash' => $document->getIdentityHash())))
			->offsetSet('language', $this->i18nService->getConfiguration()->getCurrentLocale()->getLanguage())
			->offsetSet('cardtype', $transaction->getSimplePaymentType($paymentType))
			->offsetSet('hash', $transaction->getHash($transaction));
		$this->view->assign('transaction', $transaction);
		$this->view->assign('formAction', $this->paymentTransmitter->getSettings()->getValueByPath('request.endpoints.clientApiUri'));
	}

	/**
	 * @param AbstractPaymentType $paymentType
	 * @return void
	 */
	protected function transferAssignments(AbstractPaymentType $paymentType) {
		$transaction = Transaction::create($paymentType);
		$this->view->assign('transaction', $transaction);
	}

	/**
	 * @param AbstractPaymentType $paymentType
	 * @return void
	 */
	protected function iDealTransferAssignments(AbstractPaymentType $paymentType) {
		$transaction = Transaction::create($paymentType);
		$bankGroupTypeSettings = (array)$this->paymentTransmitter->getSettings()->getValueByPath('PaymentTypes.' . $paymentType->getType() . '.bankGroupTypes');
		$bankGroupTypes = array();
		foreach ($bankGroupTypeSettings as $bankGroupType) {
			$bankGroupTypes[$bankGroupType] = $bankGroupType;
		}
		$this->view->assign('transaction', $transaction);
		$this->view->assign('bankGroupTypes', $bankGroupTypes);
	}

	/**
	 * @param AbstractPaymentType $paymentType
	 * @return void
	 */
	protected function epsTransferAssignments(AbstractPaymentType $paymentType) {
		$transaction = Transaction::create($paymentType);
		$bankGroupTypeSettings = (array)$this->paymentTransmitter->getSettings()->getValueByPath('PaymentTypes.' . $paymentType->getType() . '.bankGroupTypes');
		$bankGroupTypes = array();
		foreach ($bankGroupTypeSettings as $bankGroupType) {
			$bankGroupTypes[$bankGroupType] = $bankGroupType;
		}
		$this->view->assign('transaction', $transaction);
		$this->view->assign('bankGroupTypes', $bankGroupTypes);
	}

	/**
	 * @return void
	 * @throws AccessDeniedException
	 */
	protected function initializePaymentErrorAction() {
		$paymentTypeMappingConfiguration = $this->arguments->getArgument('paymentType')->getPropertyMappingConfiguration();
		$paymentTypeMappingConfiguration->setTypeConverterOption('TYPO3\Flow\Property\TypeConverter\ObjectConverter', ObjectConverter::CONFIGURATION_OVERRIDE_TARGET_TYPE_ALLOWED, TRUE);
		$arguments = $this->request->getArguments();
		if (!SecurityUtility::validateSaltedMd5($arguments, 'hash', 'document.__identity')) {
			throw new AccessDeniedException("Access denied", 1414682435);
		}
	}

	/**
	 * @param AbstractPaymentType $paymentType
	 * @param Invoice $document
	 * @return void
	 */
	public function paymentErrorAction(AbstractPaymentType $paymentType, Invoice $document) {
		$title = Message::SEVERITY_ERROR;
		$message = 'Unknown Error';
		if ($this->request->getHttpRequest()->hasArgument('customermessage')) {
			$message = $this->request->getHttpRequest()->getArgument('customermessage');
		}
		if ($this->request->getHttpRequest()->hasArgument('status')) {
			$title = $this->request->getHttpRequest()->getArgument('status');
		}
		$this->addFlashMessage($message, $title, Message::SEVERITY_ERROR, array(), 1414612949);
		$this->forward('new', NULL, NULL, array('paymentType' => array('__type' => $paymentType->getType()), 'document' => $document, 'hash' => $document->getIdentityHash()));
	}

	/**
	 * @throws AccessDeniedException
	 * @return void
	 */
	protected function initializeInstantTransferAuthorizationAction() {
		$paymentTypeMappingConfiguration = $this->arguments->getArgument('paymentType')->getPropertyMappingConfiguration();
		$paymentTypeMappingConfiguration->setTypeConverterOption('TYPO3\Flow\Property\TypeConverter\ObjectConverter', ObjectConverter::CONFIGURATION_OVERRIDE_TARGET_TYPE_ALLOWED, TRUE);
		$arguments = $this->request->getArguments();
		if (!SecurityUtility::validateSaltedMd5($arguments, 'hash', 'document.__identity')) {
			throw new AccessDeniedException("Access denied", 1414682436);
		}
	}

	/**
	 * @param InstantTransfer $paymentType
	 * @param Invoice $document
	 * @return void
	 */
	public function instantTransferAuthorizationAction(InstantTransfer $paymentType, Invoice $document) {

		if ($document->getIsPaid()) {
			$this->addFlashMessage('This document is already paid', Message::SEVERITY_ERROR, Message::SEVERITY_ERROR, array(), 1414687003);
			$this->redirect('show', 'Document', NULL, array('document' => $document, 'hash' => $document->getIdentityHash()));
		}

		$this->request->getMainRequest()->setArguments(array('node' => $this->request->getMainRequest()->getArgument('node')));
		$transaction = Transaction::create($paymentType);
		$transaction->offsetSet('request', 'authorization')
			->offsetSet('pseudocardpan', $paymentType->getNumber())
			->offsetSet('clearingtype', 'sb')
			->offsetSet('onlinebanktransfertype', 'PNT')
			->offsetSet('iban', $paymentType->getNumber())
			->offsetSet('bic', $paymentType->getBic())
			->offsetSet('reference', uniqid())
			->offsetSet('param', $document->getIdentity())
			->offsetSet('amount', $document->getSummaryGross()->getValue())
			->offsetSet('currency', $transaction->getSettings()->getValueByPath('currency'))
			->offsetSet('encoding', 'UTF-8')
			->offsetSet('successurl', $this->uriBuilder->setCreateAbsoluteUri(TRUE)->uriFor('paymentSuccess', array('document' => $document, 'hash' => $document->getIdentityHash())))
			->offsetSet('errorurl', $this->uriBuilder->setCreateAbsoluteUri(TRUE)->uriFor('paymentError', array('paymentType' => array('__type' => $paymentType->getType()), 'document' => $document, 'hash' => $document->getIdentityHash())))
			->offsetSet('backurl', $this->uriBuilder->setCreateAbsoluteUri(TRUE)->uriFor('show', array('document' => $document, 'hash' => $document->getIdentityHash()), 'Document'))
			->offsetSet('language', $this->i18nService->getConfiguration()->getCurrentLocale()->getLanguage())
			->offsetSet('key', md5($transaction->getSettings()->getValueByPath('security.apiKey')))
			->offsetSet('title', $document->getBookingPrimaryPersonName()->getTitle())
			->offsetSet('firstname', $document->getBookingPrimaryPersonName()->getFirstName())
			->offsetSet('lastname', $document->getBookingPrimaryPersonName()->getLastName())
			->offsetSet('company', $document->getBookingPrimaryCompany())
			->offsetSet('country', strtoupper($document->getBookingPrimaryPostal()->getCountry()->getIso2()))
			->offsetSet('email', $document->getBookingPrimaryElectronicAddress()->getIdentifier())
			->offsetSet('vatid', $document->getVat());

		$response = $this->paymentTransmitter->send($transaction, $this->paymentTransmitter->getSettings()->getValueByPath('request.endpoints.serverApiUri'), 'POST')->getResponse();
		$apiResult = new ApiResult(new ApiResponse($response));
		if ($apiResult->hasErrors()) {
			$this->apiLogger->log($apiResult->getFirstError()->getMessage(), LOG_NOTICE , $apiResult->getFirstError()->getArguments());
			$this->flashMessageContainer->addMessage($apiResult->getFirstError());
			$this->forward('new', NULL, NULL, array('paymentType' => array('__type' => $paymentType->getType()), 'document' => $document, 'hash' => $document->getIdentityHash()));
		}

		if ($apiResult->getNeedsRedirect() === TRUE) {
			$this->redirectToUri($apiResult->getRedirectUri());
		}

		$this->redirect('paymentSuccess', NULL, NULL, array('document' => $document, 'hash' => $document->getIdentityHash()));
	}

	/**
	 * @throws AccessDeniedException
	 * @return void
	 */
	protected function initializeGiroPayTransferAuthorizationAction() {
		$paymentTypeMappingConfiguration = $this->arguments->getArgument('paymentType')->getPropertyMappingConfiguration();
		$paymentTypeMappingConfiguration->setTypeConverterOption('TYPO3\Flow\Property\TypeConverter\ObjectConverter', ObjectConverter::CONFIGURATION_OVERRIDE_TARGET_TYPE_ALLOWED, TRUE);
		$arguments = $this->request->getArguments();
		if (!SecurityUtility::validateSaltedMd5($arguments, 'hash', 'document.__identity')) {
			throw new AccessDeniedException("Access denied", 1414682436);
		}
	}

	/**
	 * @param GiroPayTransfer $paymentType
	 * @param Invoice $document
	 * @return void
	 */
	public function giroPayTransferAuthorizationAction(GiroPayTransfer $paymentType, Invoice $document) {

		if ($document->getIsPaid()) {
			$this->addFlashMessage('This document is already paid', Message::SEVERITY_ERROR, Message::SEVERITY_ERROR, array(), 1414687003);
			$this->redirect('show', 'Document', NULL, array('document' => $document, 'hash' => $document->getIdentityHash()));
		}

		$this->request->getMainRequest()->setArguments(array('node' => $this->request->getMainRequest()->getArgument('node')));
		$transaction = Transaction::create($paymentType);
		$transaction->offsetSet('request', 'authorization')
			->offsetSet('pseudocardpan', $paymentType->getNumber())
			->offsetSet('clearingtype', 'sb')
			->offsetSet('onlinebanktransfertype', 'GPY')
			->offsetSet('iban', $paymentType->getNumber())
			->offsetSet('bic', $paymentType->getBic())
			->offsetSet('reference', uniqid())
			->offsetSet('param', $document->getIdentity())
			->offsetSet('amount', $document->getSummaryGross()->getValue())
			->offsetSet('currency', $transaction->getSettings()->getValueByPath('currency'))
			->offsetSet('encoding', 'UTF-8')
			->offsetSet('successurl', $this->uriBuilder->setCreateAbsoluteUri(TRUE)->uriFor('paymentSuccess', array('document' => $document, 'hash' => $document->getIdentityHash())))
			->offsetSet('errorurl', $this->uriBuilder->setCreateAbsoluteUri(TRUE)->uriFor('paymentError', array('paymentType' => array('__type' => $paymentType->getType()), 'document' => $document, 'hash' => $document->getIdentityHash())))
			->offsetSet('backurl', $this->uriBuilder->setCreateAbsoluteUri(TRUE)->uriFor('show', array('document' => $document, 'hash' => $document->getIdentityHash()), 'Document'))
			->offsetSet('language', $this->i18nService->getConfiguration()->getCurrentLocale()->getLanguage())
			->offsetSet('key', md5($transaction->getSettings()->getValueByPath('security.apiKey')))
			->offsetSet('title', $document->getBookingPrimaryPersonName()->getTitle())
			->offsetSet('firstname', $document->getBookingPrimaryPersonName()->getFirstName())
			->offsetSet('lastname', $document->getBookingPrimaryPersonName()->getLastName())
			->offsetSet('company', $document->getBookingPrimaryCompany())
			->offsetSet('country', strtoupper($document->getBookingPrimaryPostal()->getCountry()->getIso2()))
			->offsetSet('email', $document->getBookingPrimaryElectronicAddress()->getIdentifier())
			->offsetSet('vatid', $document->getVat());

		$response = $this->paymentTransmitter->send($transaction, $this->paymentTransmitter->getSettings()->getValueByPath('request.endpoints.serverApiUri'), 'POST')->getResponse();
		$apiResult = new ApiResult(new ApiResponse($response));

		if ($apiResult->hasErrors()) {
			$this->apiLogger->log($apiResult->getFirstError()->getMessage(), LOG_NOTICE , $apiResult->getFirstError()->getArguments());
			$this->flashMessageContainer->addMessage($apiResult->getFirstError());
			$this->forward('new', NULL, NULL, array('paymentType' => array('__type' => $paymentType->getType()), 'document' => $document, 'hash' => $document->getIdentityHash()));
		}

		if ($apiResult->getNeedsRedirect() === TRUE) {
			$this->redirectToUri($apiResult->getRedirectUri());
		}

		$this->redirect('paymentSuccess', NULL, NULL, array('document' => $document, 'hash' => $document->getIdentityHash()));
	}

	/**
	 * @throws AccessDeniedException
	 * @return void
	 */
	protected function initializeIDealTransferAuthorizationAction() {
		$paymentTypeMappingConfiguration = $this->arguments->getArgument('paymentType')->getPropertyMappingConfiguration();
		$paymentTypeMappingConfiguration->setTypeConverterOption('TYPO3\Flow\Property\TypeConverter\ObjectConverter', ObjectConverter::CONFIGURATION_OVERRIDE_TARGET_TYPE_ALLOWED, TRUE);
		$arguments = $this->request->getArguments();
		if (!SecurityUtility::validateSaltedMd5($arguments, 'hash', 'document.__identity')) {
			throw new AccessDeniedException("Access denied", 1414682436);
		}
	}

	/**
	 * @param IDealTransfer $paymentType
	 * @param Invoice $document
	 * @return void
	 */
	public function iDealTransferAuthorizationAction(IDealTransfer $paymentType, Invoice $document) {

		if ($document->getIsPaid()) {
			$this->addFlashMessage('This document is already paid', Message::SEVERITY_ERROR, Message::SEVERITY_ERROR, array(), 1414687003);
			$this->redirect('show', 'Document', NULL, array('document' => $document, 'hash' => $document->getIdentityHash()));
		}

		$this->request->getMainRequest()->setArguments(array('node' => $this->request->getMainRequest()->getArgument('node')));
		$transaction = Transaction::create($paymentType);
		$transaction->offsetSet('request', 'authorization')
			->offsetSet('pseudocardpan', $paymentType->getNumber())
			->offsetSet('clearingtype', 'sb')
			->offsetSet('onlinebanktransfertype', 'IDL')
			->offsetSet('iban', $paymentType->getNumber())
			->offsetSet('bic', $paymentType->getBic())
			->offsetSet('bankgrouptype', $paymentType->getBankGroupType())
			->offsetSet('reference', uniqid())
			->offsetSet('param', $document->getIdentity())
			->offsetSet('amount', $document->getSummaryGross()->getValue())
			->offsetSet('currency', $transaction->getSettings()->getValueByPath('currency'))
			->offsetSet('encoding', 'UTF-8')
			->offsetSet('successurl', $this->uriBuilder->setCreateAbsoluteUri(TRUE)->uriFor('paymentSuccess', array('document' => $document, 'hash' => $document->getIdentityHash())))
			->offsetSet('errorurl', $this->uriBuilder->setCreateAbsoluteUri(TRUE)->uriFor('paymentError', array('paymentType' => array('__type' => $paymentType->getType()), 'document' => $document, 'hash' => $document->getIdentityHash())))
			->offsetSet('backurl', $this->uriBuilder->setCreateAbsoluteUri(TRUE)->uriFor('show', array('document' => $document, 'hash' => $document->getIdentityHash()), 'Document'))
			->offsetSet('language', $this->i18nService->getConfiguration()->getCurrentLocale()->getLanguage())
			->offsetSet('key', md5($transaction->getSettings()->getValueByPath('security.apiKey')))
			->offsetSet('title', $document->getBookingPrimaryPersonName()->getTitle())
			->offsetSet('firstname', $document->getBookingPrimaryPersonName()->getFirstName())
			->offsetSet('lastname', $document->getBookingPrimaryPersonName()->getLastName())
			->offsetSet('company', $document->getBookingPrimaryCompany())
			->offsetSet('country', strtoupper($document->getBookingPrimaryPostal()->getCountry()->getIso2()))
			->offsetSet('email', $document->getBookingPrimaryElectronicAddress()->getIdentifier())
			->offsetSet('vatid', $document->getVat());

		$response = $this->paymentTransmitter->send($transaction, $this->paymentTransmitter->getSettings()->getValueByPath('request.endpoints.serverApiUri'), 'POST')->getResponse();
		$apiResult = new ApiResult(new ApiResponse($response));

		if ($apiResult->hasErrors()) {
			$this->apiLogger->log($apiResult->getFirstError()->getMessage(), LOG_NOTICE , $apiResult->getFirstError()->getArguments());
			$this->flashMessageContainer->addMessage($apiResult->getFirstError());
			$this->forward('new', NULL, NULL, array('paymentType' => array('__type' => $paymentType->getType()), 'document' => $document, 'hash' => $document->getIdentityHash()));
		}

		if ($apiResult->getNeedsRedirect() === TRUE) {
			$this->redirectToUri($apiResult->getRedirectUri());
		}

		$this->redirect('paymentSuccess', NULL, NULL, array('document' => $document, 'hash' => $document->getIdentityHash()));
	}

	/**
	 * @throws AccessDeniedException
	 * @return void
	 */
	protected function initializeEpsTransferAuthorizationAction() {
		$paymentTypeMappingConfiguration = $this->arguments->getArgument('paymentType')->getPropertyMappingConfiguration();
		$paymentTypeMappingConfiguration->setTypeConverterOption('TYPO3\Flow\Property\TypeConverter\ObjectConverter', ObjectConverter::CONFIGURATION_OVERRIDE_TARGET_TYPE_ALLOWED, TRUE);
		$arguments = $this->request->getArguments();
		if (!SecurityUtility::validateSaltedMd5($arguments, 'hash', 'document.__identity')) {
			throw new AccessDeniedException("Access denied", 1414682436);
		}
	}

	/**
	 * @param EpsTransfer $paymentType
	 * @param Invoice $document
	 * @return void
	 */
	public function epsTransferAuthorizationAction(EpsTransfer $paymentType, Invoice $document) {

		if ($document->getIsPaid()) {
			$this->addFlashMessage('This document is already paid', Message::SEVERITY_ERROR, Message::SEVERITY_ERROR, array(), 1414687003);
			$this->redirect('show', 'Document', NULL, array('document' => $document, 'hash' => $document->getIdentityHash()));
		}

		$this->request->getMainRequest()->setArguments(array('node' => $this->request->getMainRequest()->getArgument('node')));
		$transaction = Transaction::create($paymentType);
		$transaction->offsetSet('request', 'authorization')
			->offsetSet('pseudocardpan', $paymentType->getNumber())
			->offsetSet('clearingtype', 'sb')
			->offsetSet('onlinebanktransfertype', 'EPS')
			->offsetSet('iban', $paymentType->getNumber())
			->offsetSet('bic', $paymentType->getBic())
			->offsetSet('bankgrouptype', $paymentType->getBankGroupType())
			->offsetSet('reference', uniqid())
			->offsetSet('param', $document->getIdentity())
			->offsetSet('amount', $document->getSummaryGross()->getValue())
			->offsetSet('currency', $transaction->getSettings()->getValueByPath('currency'))
			->offsetSet('encoding', 'UTF-8')
			->offsetSet('successurl', $this->uriBuilder->setCreateAbsoluteUri(TRUE)->uriFor('paymentSuccess', array('document' => $document, 'hash' => $document->getIdentityHash())))
			->offsetSet('errorurl', $this->uriBuilder->setCreateAbsoluteUri(TRUE)->uriFor('paymentError', array('paymentType' => array('__type' => $paymentType->getType()), 'document' => $document, 'hash' => $document->getIdentityHash())))
			->offsetSet('backurl', $this->uriBuilder->setCreateAbsoluteUri(TRUE)->uriFor('show', array('document' => $document, 'hash' => $document->getIdentityHash()), 'Document'))
			->offsetSet('language', $this->i18nService->getConfiguration()->getCurrentLocale()->getLanguage())
			->offsetSet('key', md5($transaction->getSettings()->getValueByPath('security.apiKey')))
			->offsetSet('title', $document->getBookingPrimaryPersonName()->getTitle())
			->offsetSet('firstname', $document->getBookingPrimaryPersonName()->getFirstName())
			->offsetSet('lastname', $document->getBookingPrimaryPersonName()->getLastName())
			->offsetSet('company', $document->getBookingPrimaryCompany())
			->offsetSet('country', strtoupper($document->getBookingPrimaryPostal()->getCountry()->getIso2()))
			->offsetSet('email', $document->getBookingPrimaryElectronicAddress()->getIdentifier())
			->offsetSet('vatid', $document->getVat());

		$response = $this->paymentTransmitter->send($transaction, $this->paymentTransmitter->getSettings()->getValueByPath('request.endpoints.serverApiUri'), 'POST')->getResponse();
		$apiResult = new ApiResult(new ApiResponse($response));

		if ($apiResult->hasErrors()) {
			$this->apiLogger->log($apiResult->getFirstError()->getMessage(), LOG_NOTICE , $apiResult->getFirstError()->getArguments());
			$this->flashMessageContainer->addMessage($apiResult->getFirstError());
			$this->forward('new', NULL, NULL, array('paymentType' => array('__type' => $paymentType->getType()), 'document' => $document, 'hash' => $document->getIdentityHash()));
		}

		if ($apiResult->getNeedsRedirect() === TRUE) {
			$this->redirectToUri($apiResult->getRedirectUri());
		}

		$this->redirect('paymentSuccess', NULL, NULL, array('document' => $document, 'hash' => $document->getIdentityHash()));
	}

	/**
	 * @throws AccessDeniedException
	 * @return void
	 */
	protected function initializeCreditCardAuthorizationAction() {
		$paymentTypeMappingConfiguration = $this->arguments->getArgument('paymentType')->getPropertyMappingConfiguration();
		$paymentTypeMappingConfiguration->setTypeConverterOption('TYPO3\Flow\Property\TypeConverter\ObjectConverter', ObjectConverter::CONFIGURATION_OVERRIDE_TARGET_TYPE_ALLOWED, TRUE);
		$arguments = $this->request->getArguments();
		if (!SecurityUtility::validateSaltedMd5($arguments, 'hash', 'document.__identity')) {
			throw new AccessDeniedException("Access denied", 1414682436);
		}
	}

	/**
	 * @param AbstractCreditCard $paymentType
	 * @param Invoice $document
	 * @return void
	 */
	public function creditCardAuthorizationAction(AbstractCreditCard $paymentType, Invoice $document) {

		if ($document->getIsPaid()) {
			$this->addFlashMessage('This document is already paid', Message::SEVERITY_ERROR, Message::SEVERITY_ERROR, array(), 1414687003);
			$this->redirect('show', 'Document', NULL, array('document' => $document, 'hash' => $document->getIdentityHash()));
		}

		$this->request->getMainRequest()->setArguments(array('node' => $this->request->getMainRequest()->getArgument('node')));
		$paymentType->setNumber((integer)$this->request->getHttpRequest()->getArgument('pseudocardpan'));
		$transaction = Transaction::create($paymentType);
		$transaction->offsetSet('request', 'authorization')
			->offsetSet('pseudocardpan', $paymentType->getNumber())
			->offsetSet('clearingtype', 'cc')
			->offsetSet('reference', uniqid())
			->offsetSet('param', $document->getIdentity())
			->offsetSet('amount', $document->getSummaryGross()->getValue())
			->offsetSet('currency', $transaction->getSettings()->getValueByPath('currency'))
			->offsetSet('encoding', 'UTF-8')
			->offsetSet('successurl', $this->uriBuilder->setCreateAbsoluteUri(TRUE)->uriFor('paymentSuccess', array('document' => $document, 'hash' => $document->getIdentityHash())))
			->offsetSet('errorurl', $this->uriBuilder->setCreateAbsoluteUri(TRUE)->uriFor('paymentError', array('paymentType' => array('__type' => $paymentType->getType()), 'document' => $document, 'hash' => $document->getIdentityHash())))
			->offsetSet('backurl', $this->uriBuilder->setCreateAbsoluteUri(TRUE)->uriFor('show', array('document' => $document, 'hash' => $document->getIdentityHash()), 'Document'))
			->offsetSet('language', $this->i18nService->getConfiguration()->getCurrentLocale()->getLanguage())
			->offsetSet('key', md5($transaction->getSettings()->getValueByPath('security.apiKey')))
			->offsetSet('title', $document->getBookingPrimaryPersonName()->getTitle())
			->offsetSet('firstname', $document->getBookingPrimaryPersonName()->getFirstName())
			->offsetSet('lastname', $document->getBookingPrimaryPersonName()->getLastName())
			->offsetSet('company', $document->getBookingPrimaryCompany())
			->offsetSet('country', strtoupper($document->getBookingPrimaryPostal()->getCountry()->getIso2()))
			->offsetSet('email', $document->getBookingPrimaryElectronicAddress()->getIdentifier())
			->offsetSet('vatid', $document->getVat());

		$response = $this->paymentTransmitter->send($transaction, $this->paymentTransmitter->getSettings()->getValueByPath('request.endpoints.serverApiUri'), 'POST')->getResponse();
		$apiResult = new ApiResult(new ApiResponse($response));

		if ($apiResult->hasErrors()) {
			$this->apiLogger->log($apiResult->getFirstError()->getMessage(), LOG_NOTICE , $apiResult->getFirstError()->getArguments());
			$this->flashMessageContainer->addMessage($apiResult->getFirstError());
			$this->forward('new', NULL, NULL, array('paymentType' => array('__type' => $paymentType->getType()), 'document' => $document, 'hash' => $document->getIdentityHash()));
		}

		if ($apiResult->getNeedsRedirect() === TRUE) {
			$this->redirectToUri($apiResult->getRedirectUri());
		}

		$this->redirect('paymentSuccess', NULL, NULL, array('document' => $document, 'hash' => $document->getIdentityHash()));
	}

	/**
	 * @return void
	 * @throws AccessDeniedException
	 */
	protected function initializePaymentSuccessAction() {
		$arguments = $this->request->getArguments();
		if (!SecurityUtility::validateSaltedMd5($arguments, 'hash', 'document.__identity')) {
			throw new AccessDeniedException("Access denied", 1414682437);
		}
	}

	/**
	 * @param Invoice $document
	 * @return void
	 */
	public function paymentSuccessAction(Invoice $document) {
		$this->addFlashMessage('Payment Process finished. Delivery will start right after your payment finally arrives.', 'Thank you', Message::SEVERITY_OK, array(), 1414669954);
		$this->view->assign('document', $document);
	}
}
