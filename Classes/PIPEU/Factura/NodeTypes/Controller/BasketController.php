<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\NodeTypes\Controller;

use PIPEU\Factura\NodeTypes\Controller\Abstracts\AbstractPluginController;
use PIPEU\Factura\Security\Utility as SecurityUtility;
use PIPEU\Factura\Validation\Validator\Documents\OrderValidator;
use TYPO3\Flow\Error\Message;
use TYPO3\Flow\Exception;
use TYPO3\Flow\I18n\Service as I18nService;
use PIPEU\Factura\Domain\Interfaces\InterfaceDelivery;
use PIPEU\Factura\Domain\Interfaces\InterfaceFacturaItem;
use PIPEU\Factura\Domain\Model\Documents\Order;
use PIPEU\Factura\NodeTypes\Domain\Model\DeliveryDto;
use PIPEU\Factura\NodeTypes\Domain\Model\InvoiceDto;
use PIPEU\Factura\Basket\Session\Storage;
use PIPEU\Factura\Domain\Abstracts\AbstractFacturaItem;
use PIPEU\Factura\NodeTypes\Aspect\ContextPathInterface;
use PIPEU\Factura\Domain\Model\Unit;
use PIPEU\Geo\Domain\Model\Interfaces\InterfacePostal;
use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Mvc\Controller\ControllerContext;
use TYPO3\Flow\Property\TypeConverter\PersistentObjectConverter;
use TYPO3\Flow\Reflection\ObjectAccess;
use Doctrine\Common\Collections\ArrayCollection;
use PIPEU\Geo\Domain\Repository\CountryRepository;
use TYPO3\Flow\Security\Exception\AccessDeniedException;
use TYPO3\Flow\Utility\Arrays;
use TYPO3\Party\Domain\Model\ElectronicAddress;
use TYPO3\Flow\Property\PropertyMapper;
use TYPO3\Flow\Error\Result as ValidationResults;
use TYPO3\Party\Domain\Model\PersonName;

/**
 * Class BasketController
 *
 * @package PIPEU\Factura\NodeTypes\Controller
 */
class BasketController extends AbstractPluginController {

	/**
	 * @var PropertyMapper
	 * @Flow\Inject
	 */
	protected $propertyMapper;

	/**
	 * @var I18nService
	 * @Flow\Inject
	 */
	protected $i18nService;

	/**
	 * @var Storage
	 * @Flow\Inject
	 */
	protected $storage;

	/**
	 * @var CountryRepository
	 * @Flow\Inject
	 */
	protected $countryRepository;

	/**
	 * @var ValidationResults
	 */
	protected $validationResults;

	/**
	 * @return void
	 */
	protected function initializeIndexAction() {
	}

	/**
	 * @return void
	 */
	public function indexAction() {
		$this->view->assign('basket', $this->storage);
	}

	/**
	 * @return void
	 */
	protected function initializeUpdateAction() {
	}

	/**
	 * @param \Doctrine\Common\Collections\ArrayCollection<Unit> $values
	 * @param boolean $continue
	 * @return void
	 */
	public function updateAction(ArrayCollection $values, $continue = FALSE) {

		$iterator = $values->getIterator();

		while ($iterator->valid()) {

			/** @var Unit $unit */
			$unit = $iterator->current();

			/** @var AbstractFacturaItem $facturaItemFilteredByContextPath */
			$facturaItemFilteredByContextPath = $this->storage->getOrder()->getFacturaItems()->filter($this->getNodeContextPathFilter($iterator->key()))->first();

			if ($unit->getValue() === 0) {
				$this->storage->getOrder()->removeFacturaItem($facturaItemFilteredByContextPath);
			} else {
				$facturaItemFilteredByContextPath->setUnit($unit);
				$this->setFacturaItem($facturaItemFilteredByContextPath);
			}

			$iterator->next();
		}

		$this->removeDeliveryIfNecessary();
		$this->moveDeliveryToBottom();

		if ($continue === TRUE) {
			$this->redirect('invoice');
		} else {
			$this->redirect('index');
		}
	}

	/**
	 * @return void
	 */
	protected function initializeInvoiceAction() {
	}

	/**
	 * @return void
	 */
	public function invoiceAction() {
		$existingData = $this->storage->getOrder()->getPrimaryPostal() instanceof InterfacePostal || $this->storage->getOrder()->getPrimaryPersonName() instanceof PersonName
			|| $this->storage->getOrder()->getVat() !== NULL || $this->storage->getOrder()->getPrimaryElectronicAddress() instanceof ElectronicAddress;

		if ($existingData === TRUE) {
			$dto = new InvoiceDto(
				$this->storage->getOrder()->getPrimaryPostal(),
				$this->storage->getOrder()->getPrimaryPersonName(),
				$this->storage->getOrder()->getVat(),
				$this->storage->getOrder()->getPrimaryElectronicAddress(),
				$this->storage->getOrder()->getPrimaryCompany()
			);
			$this->view->assign('dto', $dto);
		}
		$this->view->assign('countries', $this->countryRepository->findAll());
	}

	/**
	 * @return void
	 */
	protected function initializeUpdateInvoiceAction() {
		$requestArguments = Arrays::setValueByPath($this->request->getArguments(), 'dto.electronicAddress.type', ElectronicAddress::TYPE_EMAIL);
		$this->request->setArguments($requestArguments);

		$dtoPropertyMappingConfiguration = $this->getControllerContext()->getArguments()->getArgument('dto')->getPropertyMappingConfiguration();
		$dtoPropertyMappingConfiguration->forProperty('electronicAddress')->allowProperties('type');
		$dtoPropertyMappingConfiguration->forProperty('postal')->setTypeConverterOption('TYPO3\Flow\Property\TypeConverter\PersistentObjectConverter', PersistentObjectConverter::CONFIGURATION_TARGET_TYPE, 'PIPEU\Geo\Domain\Model\Postal');

		if ($this->request->hasArgument('back') && (boolean)$this->request->getArgument('back')) {
			$dtoArgument = $this->arguments->getArgument('dto');
			ObjectAccess::setProperty($dtoArgument, 'validator', NULL, TRUE);
		}
	}

	/**
	 * @Flow\Validate(argumentName="dto", type="PIPEU\Factura\NodeTypes\Validation\Validator\Dto\InvoiceDtoValidator")
	 * @param InvoiceDto $dto
	 * @param boolean $continue
	 * @param boolean $back
	 *
	 * @return void
	 */
	public function updateInvoiceAction(InvoiceDto $dto, $continue = FALSE, $back = FALSE) {
		$this->storage->getOrder()->setPrimaryPostal($dto->getPostal());
		$this->storage->getOrder()->setPrimaryPersonName($dto->getPersonName());
		$this->storage->getOrder()->setVat($dto->getVat());
		$this->storage->getOrder()->setPrimaryElectronicAddress($dto->getElectronicAddress());
		$this->storage->getOrder()->setPrimaryCompany($dto->getCompany());

		if ($continue === TRUE) {
			$this->redirect('delivery');
		} elseif ($back === TRUE) {
			$this->redirect('index');
		} else {
			$this->redirect('invoice');
		}
	}

	/**
	 * @return void
	 */
	protected function initializeDeliveryAction() {
	}

	/**
	 * @return void
	 */
	public function deliveryAction() {
		if ($this->storage->getOrder()->getSecondaryPostal() instanceof InterfacePostal || $this->storage->getOrder()->getSecondaryPersonName() instanceof PersonName) {
			$dto = new DeliveryDto(
				$this->storage->getOrder()->getSecondaryPostal(),
				$this->storage->getOrder()->getSecondaryPersonName(),
				$this->storage->getOrder()->getSecondaryCompany()
			);
			$this->view->assign('dto', $dto);
		} elseif ($this->storage->getOrder()->getPrimaryPostal() instanceof InterfacePostal || $this->storage->getOrder()->getPrimaryPersonName() instanceof PersonName) {
			$dto = new DeliveryDto(
				$this->storage->getOrder()->getPrimaryPostal(),
				$this->storage->getOrder()->getPrimaryPersonName(),
				$this->storage->getOrder()->getPrimaryCompany()
			);
			$this->view->assign('dto', $dto);
		}
		$this->view->assign('countries', $this->countryRepository->findAll());
	}

	/**
	 * @return void
	 */
	protected function initializeUpdateDeliveryAction() {
		$dtoPropertyMappingConfiguration = $this->getControllerContext()->getArguments()->getArgument('dto')->getPropertyMappingConfiguration();
		$dtoPropertyMappingConfiguration->forProperty('postal')->setTypeConverterOption('TYPO3\Flow\Property\TypeConverter\PersistentObjectConverter', PersistentObjectConverter::CONFIGURATION_TARGET_TYPE, 'PIPEU\Geo\Domain\Model\Postal');
		if ($this->request->hasArgument('back') && (boolean)$this->request->getArgument('back')) {
			$dtoArgument = $this->arguments->getArgument('dto');
			ObjectAccess::setProperty($dtoArgument, 'validator', NULL, TRUE);
		}
	}

	/**
	 * @Flow\Validate(argumentName="dto", type="PIPEU\Factura\NodeTypes\Validation\Validator\Dto\DeliveryDtoValidator")
	 * @param DeliveryDto $dto
	 * @param boolean $continue
	 * @param boolean $back
	 *
	 * @return void
	 */
	public function updateDeliveryAction(DeliveryDto $dto, $continue = FALSE, $back = FALSE) {
		$this->storage->getOrder()->setSecondaryPostal($dto->getPostal());
		$this->storage->getOrder()->setSecondaryPersonName($dto->getPersonName());
		$this->storage->getOrder()->setSecondaryCompany($dto->getCompany());
		$this->removeDeliveryIfNecessary();

		if ($continue === TRUE) {
			$this->redirect('provider');
		} elseif ($back === TRUE) {
			$this->redirect('invoice');
		} else {
			$this->redirect('delivery');
		}
	}

	/**
	 * @return void
	 */
	protected function initializeProviderAction() {
	}

	/**
	 * @return void
	 */
	public function providerAction() {
		$this->view->assign('basket', $this->storage);
	}

	/**
	 * @return void
	 */
	protected function initializeUpdateProviderAction() {
		if ($this->request->hasArgument('back') && (boolean)$this->request->getArgument('back')) {
			$deliveryArgument = $this->arguments->getArgument('delivery');
			ObjectAccess::setProperty($deliveryArgument, 'validator', NULL, TRUE);
		}
	}

	/**
	 * @param InterfaceDelivery $delivery
	 * @param boolean $continue
	 * @param boolean $back
	 *
	 * @return void
	 */
	public function updateProviderAction(InterfaceDelivery $delivery = NULL, $continue = FALSE, $back = FALSE) {
		if ($delivery instanceof InterfaceDelivery) {
			$this->setFacturaItem($delivery);
		}
		if ($continue === TRUE) {
			$this->redirect('control');
		} elseif ($back === TRUE) {
			$this->redirect('delivery');
		} else {
			$this->redirect('provider');
		}
	}

	/**
	 * @return void
	 */
	protected function initializeControlAction() {
	}

	/**
	 * @return void
	 */
	public function controlAction() {
		$this->view->assign('basket', $this->storage);
		$this->view->assign('termsAndConditionNode', $this->request->getInternalArgument('__termsAndConditionNode'));
	}

	/**
	 * @return void
	 */
	protected function initializeOrderAction() {
	}

	/**
	 * @param boolean $termsAndConditionsAccepted
	 * @param boolean $back
	 *
	 * @Flow\Validate(argumentName="termsAndConditionsAccepted", type="BooleanValue", options={"expectedValue"=true})
	 * @return void
	 */
	public function orderAction($termsAndConditionsAccepted, $back = FALSE) {
		if ($back === TRUE) {
			$this->redirect('provider');
		}

		$order = $this->storage->getOrder();
		$order->setDateTime(new \DateTime());
		$order->setLocaleIdentifier((string)$this->i18nService->getConfiguration()->getCurrentLocale());

		$validator = new OrderValidator();
		$validationResults = $validator->validate($order);

		if ($validationResults->hasErrors()) {
			$this->validationResults = $validationResults;
			$this->addFlashMessage('Validation failed.', Message::SEVERITY_ERROR, Message::SEVERITY_ERROR, array(), 1414839362);
			call_user_func_array(array($this, 'orderErrorAction'), array('validationResults' => $validationResults));
		}

		if ($this->persistenceManager->isNewObject($order)) {
			$this->persistenceManager->add($order);
		} else {
			$this->persistenceManager->update($order);
		}

		$this->forward('finish', NULL, NULL, array('order' => $order, 'hash' => $order->getIdentityHash()));
	}

	/**
	 * @param ValidationResults $validationResults
	 *
	 * @return void
	 */
	protected function orderErrorAction(ValidationResults $validationResults) {
		$actionName = $this->resolveActionNameByValidationResults($validationResults);
		$argumentsForNextController = ['__submittedArguments' => $this->resolveSubmittedArgumentsForActionName($actionName), '__submittedArgumentValidationResults' => $this->resolveSubmittedArgumentValidationResultsForActionName($actionName, $validationResults)];
		$this->forward($actionName, NULL, NULL, $argumentsForNextController);
	}

	/**
	 * @param string $actionName
	 *
	 * @return array
	 */
	protected function resolveSubmittedArgumentsForActionName($actionName) {
		$result = array();
		$order = $this->storage->getOrder();
		$pathMapping = new \ArrayObject();

		if ($actionName === 'invoice') {
			$pathMapping->offsetSet('vat', 'dto.vat');
			$pathMapping->offsetSet('primaryCompany', 'dto.company');
			$pathMapping->offsetSet('primaryPostal.address', 'dto.postal.address');
			$pathMapping->offsetSet('primaryPostal.code', 'dto.postal.code');
			$pathMapping->offsetSet('primaryPostal.city', 'dto.postal.city');
			$pathMapping->offsetSet('primaryPostal.country.iso2', 'dto.postal.country');
			$pathMapping->offsetSet('primaryPostal.county', 'dto.postal.county');
			$pathMapping->offsetSet('primaryPersonName.title', 'dto.personName.title');
			$pathMapping->offsetSet('primaryPersonName.firstName', 'dto.personName.firstName');
			$pathMapping->offsetSet('primaryPersonName.lastName', 'dto.personName.lastName');
			$pathMapping->offsetSet('primaryElectronicAddress.identifier', 'dto.electronicAddress.identifier');
		}

		if ($actionName === 'delivery') {
			$pathMapping->offsetSet('secondaryCompany', 'dto.company');
			$pathMapping->offsetSet('secondaryPostal.address', 'dto.postal.address');
			$pathMapping->offsetSet('secondaryPostal.code', 'dto.postal.code');
			$pathMapping->offsetSet('secondaryPostal.city', 'dto.postal.city');
			$pathMapping->offsetSet('secondaryPostal.country.iso2', 'dto.postal.country');
			$pathMapping->offsetSet('secondaryPostal.county', 'dto.postal.county');
			$pathMapping->offsetSet('secondaryPersonName.title', 'dto.personName.title');
			$pathMapping->offsetSet('secondaryPersonName.firstName', 'dto.personName.firstName');
			$pathMapping->offsetSet('secondaryPersonName.lastName', 'dto.personName.lastName');
		}

		$pathMappingIterator = $pathMapping->getIterator();
		while ($pathMappingIterator->valid()) {
			$sourcePath = $pathMappingIterator->key();
			$targetPath = $pathMappingIterator->current();
			$value = ObjectAccess::getPropertyPath($order, $sourcePath);
			$result = Arrays::setValueByPath($result, $targetPath, $value);
			$pathMappingIterator->next();
		}

		return $result;
	}

	/**
	 * @param string $actionName
	 * @param ValidationResults $validationResults
	 *
	 * @return ValidationResults
	 */
	protected function resolveSubmittedArgumentValidationResultsForActionName($actionName, ValidationResults $validationResults) {
		$result = new ValidationResults();
		if ($actionName === 'invoice') {
			$result->forProperty('dto.personName')->merge($validationResults->forProperty('primaryPersonName'));
			$result->forProperty('dto.postal')->merge($validationResults->forProperty('primaryPostal'));
			$result->forProperty('dto.electronicAddress')->merge($validationResults->forProperty('primaryElectronicAddress'));
			$result->forProperty('dto.vat')->merge($validationResults->forProperty('vat'));
			$result->forProperty('dto.company')->merge($validationResults->forProperty('primaryCompany'));
		}
		if ($actionName === 'delivery') {
			$result->forProperty('dto.personName')->merge($validationResults->forProperty('secondaryPersonName'));
			$result->forProperty('dto.postal')->merge($validationResults->forProperty('secondaryPostal'));
			$result->forProperty('dto.company')->merge($validationResults->forProperty('secondaryCompany'));
		}
		if ($actionName === 'provider') {
			$result->forProperty('deliveryProvider')->merge($validationResults->forProperty('facturaItems.deliveryProvider'));
		}
		return $result;
	}

	/**
	 * @param ValidationResults $validationResults
	 *
	 * @return string
	 * @throws Exception
	 */
	protected function resolveActionNameByValidationResults(ValidationResults $validationResults) {
		$propertyPath = 'facturaItems.products';
		if ($validationResults->forProperty($propertyPath)->hasErrors()) {
			return 'index';
		}

		$propertyPath = 'primaryPersonName';
		if ($validationResults->forProperty($propertyPath)->hasErrors()) {
			return 'invoice';
		}

		$propertyPath = 'primaryElectronicAddress';
		if ($validationResults->forProperty($propertyPath)->hasErrors()) {
			return 'invoice';
		}

		$propertyPath = 'primaryPostal';
		if ($validationResults->forProperty($propertyPath)->hasErrors()) {
			return 'invoice';
		}

		$propertyPath = 'secondaryPersonName';
		if ($validationResults->forProperty($propertyPath)->hasErrors()) {
			return 'delivery';
		}

		$propertyPath = 'secondaryPostal';
		if ($validationResults->forProperty($propertyPath)->hasErrors()) {
			return 'delivery';
		}

		$propertyPath = 'facturaItems.deliveryProvider';
		if ($validationResults->forProperty($propertyPath)->hasErrors()) {
			return 'provider';
		}

		$propertyPath = 'vat';
		if ($validationResults->forProperty($propertyPath)->hasErrors()) {
			return 'invoice';
		}

		throw new Exception('Can not resolve actionName by validation results', 1401555047);
	}

	/**
	 * @throws AccessDeniedException
	 * @return void
	 */
	protected function initializeFinishAction() {
		$arguments = $this->request->getArguments();
		if(!SecurityUtility::validateSaltedMd5($arguments, 'hash', 'order.__identity')){
			throw new AccessDeniedException("Access denied", 1401652990);
		}
	}

	/**
	 * @param Order $order
	 *
	 * @return void
	 */
	public function finishAction(Order $order) {
		$this->storage->prune();
		$this->emitOrderCreated($order, $this->controllerContext);
		$this->redirect('show', 'Document', NULL, array('document' => $order, 'hash' => $order->getIdentityHash()));
	}

	/**
	 * @param Order $order
	 * @param ControllerContext $controllerContext
	 *
	 * @return void
	 * @Flow\Signal
	 */
	protected function emitOrderCreated(Order $order, ControllerContext $controllerContext) {
	}

	/**
	 * @return void
	 */
	public function initializeAddFacturaItemAction() {
		$propertyMappingConfiguration = $this->controllerContext->getArguments()->getArgument('item')->getPropertyMappingConfiguration();
		$propertyMappingConfiguration->setTypeConverterOption('TYPO3\Flow\Property\TypeConverter\PersistentObjectConverter', \TYPO3\Flow\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_CREATION_ALLOWED, TRUE);
		$propertyMappingConfiguration->setTypeConverterOption('TYPO3\Flow\Property\TypeConverter\PersistentObjectConverter', \TYPO3\Flow\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_MODIFICATION_ALLOWED, TRUE);
		$propertyMappingConfiguration->allowAllProperties();
	}

	/**
	 * @param AbstractFacturaItem $item
	 * @param Unit $unit
	 *
	 * @return void
	 */
	public function addFacturaItemAction(AbstractFacturaItem $item, Unit $unit) {
		$this->updateFacturaItem($item, $unit);
		$referringRequest = $this->request->getMainRequest()->getReferringRequest();
		$this->forwardToRequest($referringRequest);
	}

	/**
	 * @param InterfaceFacturaItem $item
	 * @param Unit $unit
	 *
	 * @return $this
	 */
	protected function updateFacturaItem(InterfaceFacturaItem $item, Unit $unit) {
		if ($item instanceof InterfaceDelivery) {
			$existingFacturaItems = $this->storage->getOrder()->getFacturaItems()->filter($this->getDeliveryByTypeFilter($item->getType()));
		} else {
			$existingFacturaItems = $this->storage->getOrder()->getFacturaItems()->filter($this->getNodeContextPathFilter($item->getContextPath()));
		}
		/** @var InterfaceFacturaItem $existingFacturaItem */
		foreach ($existingFacturaItems as $existingFacturaItem) {
			$calcedUnit = $unit->getValue() + $existingFacturaItem->getUnit()->getValue();
			ObjectAccess::setProperty($unit, 'value', $calcedUnit, TRUE);
		}
		$this->setFacturaItem($item->setUnit($unit));
		return $this;
	}

	/**
	 * @param InterfaceFacturaItem $item
	 *
	 * @return $this
	 */
	protected function setFacturaItem(InterfaceFacturaItem $item) {
		if ($item instanceof InterfaceDelivery) {
			$existingFacturaItems = $this->storage->getOrder()->getFacturaItems()->filter(self::getDeliveryFilter($item->getType()));
		} else {
			$existingFacturaItems = $this->storage->getOrder()->getFacturaItems()->filter($this->getNodeContextPathFilter($item->getContextPath()));
		}
		/** @var InterfaceFacturaItem $existingFacturaItem */
		foreach ($existingFacturaItems as $existingFacturaItem) {
			$this->storage->getOrder()->removeFacturaItem($existingFacturaItem);
		}
		$this->storage->getOrder()->addFacturaItem($item);
		$this->moveDeliveryToBottom()->removeDeliveryIfNecessary();
		return $this;
	}

	/**
	 * @param string $contextPath
	 *
	 * @return callable
	 */
	protected function getNodeContextPathFilter($contextPath) {
		return function (InterfaceFacturaItem $facturaItem) use ($contextPath) {
			return $facturaItem instanceof ContextPathInterface && $facturaItem->getContextPath() === $contextPath;
		};
	}

	/**
	 * @param string $type
	 *
	 * @return callable
	 */
	public static function getDeliveryFilter($type) {
		return function (InterfaceFacturaItem $facturaItem) use ($type) {
			return $facturaItem instanceof InterfaceDelivery;
		};
	}

	/**
	 * @return $this
	 */
	protected function removeDeliveryIfNecessary() {

		$possibleDeliveries = $this->storage->getOrder()->getPossibleDeliveries();
		$currentDeliveries = $this->storage->getOrder()->getFacturaItems()->filter(Storage::getDeliveryFilter());

		/** @var InterfaceDelivery $currentDelivery */
		foreach ($currentDeliveries as $currentDelivery) {
			$isValid = FALSE;
			/** @var InterfaceDelivery $possibleDelivery */
			foreach ($possibleDeliveries as $possibleDelivery) {
				if ($possibleDelivery->getType() === $currentDelivery->getType() && $possibleDelivery->getPrice()->getValue() === $currentDelivery->getPrice()->getValue() && $possibleDelivery->getTax()->getValue() === $currentDelivery->getTax()->getValue()) {
					$isValid = TRUE;
				}
			}
			if ($isValid === FALSE) {
				$this->addFlashMessage('Your selected delivery provider does not match anymore to your order. Please select a new provider during next steps.', 'Please note', Message::SEVERITY_NOTICE, array(), 1412510973);
				$this->storage->getOrder()->removeFacturaItem($currentDelivery);
			}
		}

		return $this;
	}

	/**
	 * @return $this
	 */
	protected function moveDeliveryToBottom() {
		$currentDeliveries = $this->storage->getOrder()->getFacturaItems()->filter(Storage::getDeliveryFilter());
		/** @var InterfaceDelivery $currentDelivery */
		foreach ($currentDeliveries as $currentDelivery) {
			$this->storage->getOrder()->removeFacturaItem($currentDelivery);
			$this->storage->getOrder()->addFacturaItem($currentDelivery);
		}
		return $this;
	}
}
