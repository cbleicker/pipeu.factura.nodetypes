<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\NodeTypes\ViewHelpers;

use PIPEU\Factura\NodeTypes\Aspect\ContextPathInterface;
use TYPO3\Fluid\Core\ViewHelper\AbstractViewHelper;
use PIPEU\Factura\Basket\Session\Storage;
use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Persistence\PersistenceManagerInterface;
use TYPO3\TYPO3CR\Domain\Model\Node;

/**
 * Class StorageContainsNodeViewHelper
 *
 * @package PIPEU\Factura\NodeTypes\ViewHelpers
 */
class StorageContainsNodeViewHelper extends AbstractViewHelper {

	/**
	 * @var Storage
	 * @Flow\Inject
	 */
	protected $storage;

	/**
	 * @var PersistenceManagerInterface
	 * @Flow\Inject
	 */
	protected $persistenceManager;

	/**
	 * @param Node $node
	 * @return boolean
	 * @throws \InvalidArgumentException
	 */
	public function render(Node $node = NULL) {
		if ($node === NULL) {
			$node = $this->renderChildren();
		}
		if (!($node instanceof Node)) {
			throw new \InvalidArgumentException('Argument node must be of type of "\TYPO3\TYPO3CR\Domain\Model\Node" but "' . gettype($node) . '" given', 1402307863);
		}
		return (boolean)$this->storage->getOrder()->getFacturaItems()->filter($this->getFilterFunction($node))->count();
	}

	/**
	 * @param Node $node
	 * @return \Closure
	 */
	protected function getFilterFunction(Node $node) {
		return function (ContextPathInterface $facturaItem) use ($node) {
			return $facturaItem->getContextPath() === $node->getContextPath();
		};
	}
}
