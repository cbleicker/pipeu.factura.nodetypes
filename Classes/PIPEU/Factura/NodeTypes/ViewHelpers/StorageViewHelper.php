<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\NodeTypes\ViewHelpers;

use PIPEU\Factura\Basket\Session\Storage;
use TYPO3\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3\Flow\Annotations as Flow;

/**
 * Class StorageViewHelper
 *
 * @package PIPEU\Factura\NodeTypes\ViewHelpers\Format
 */
class StorageViewHelper extends AbstractViewHelper {

	/**
	 * @var Storage
	 * @Flow\Inject
	 */
	protected $storage;

	/**
	 * @param string $as
	 * @return mixed
	 */
	public function render($as = 'storage') {
		$this->templateVariableContainer->add($as, $this->storage);
		$output = $this->renderChildren();
		$this->templateVariableContainer->remove($as);
		return $output;
	}
}