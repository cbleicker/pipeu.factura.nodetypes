<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\NodeTypes\ViewHelpers\Format;

use TYPO3\Flow\Annotations as Flow;
use TYPO3\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3\TYPO3CR\Utility;

/**
 * Class ValidParameterViewHelper
 *
 * @package PIPEU\Factura\NodeTypes\ViewHelpers\Format
 * @Flow\Scope("singleton")
 */
class ValidParameterViewHelper extends AbstractViewHelper {

	/**
	 * @param string $value
	 * @return string
	 * @throws \InvalidArgumentException
	 */
	public function render($value = NULL) {
		if ($value === NULL) {
			$value = $this->renderChildren();
		}
		if (!is_string($value)) {
			throw new \InvalidArgumentException('Only strings allowed to be converted to a valid parameter', 1415011232);
		}
		return Utility::renderValidNodeName($value);
	}
}

