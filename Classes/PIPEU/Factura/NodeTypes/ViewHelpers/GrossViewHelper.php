<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\NodeTypes\ViewHelpers;

use TYPO3\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Class GrossViewHelper
 *
 * @package PIPEU\Factura\NodeTypes\ViewHelpers
 */
class GrossViewHelper extends AbstractViewHelper {

	const DECIMAL_POINTS = 2;

	/**
	 * @param float $net
	 * @param float $tax
	 * @return float
	 */
	public function render($net = NULL, $tax = NULL) {
		if ($net === NULL) {
			$net = $this->renderChildren();
		}

		$net = $this->getRounded($net);
		$tax = $this->getRounded($tax);
		$percent = ($tax + 100) / 100;
		$gross = $net * $percent;
		$gross = $this->getRounded($gross);

		return $gross;
	}

	/**
	 * @param float $value
	 * @return float
	 */
	protected function getRounded($value) {
		$factor = pow(10, static::DECIMAL_POINTS);
		$value = (float)$value;
		$value = $value * $factor;
		$value = (integer)$value;
		$value = $value / $factor;
		return $value;
	}
}
