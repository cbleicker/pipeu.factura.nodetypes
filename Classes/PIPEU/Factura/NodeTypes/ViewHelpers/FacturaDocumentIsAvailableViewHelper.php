<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\NodeTypes\ViewHelpers;

use TYPO3\Flow\Exception;
use TYPO3\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3\TYPO3CR\Domain\Model\NodeInterface;

/**
 * Class FacturaDocumentIsAvailableViewHelper
 *
 * @package PIPEU\Factura\NodeTypes\ViewHelpers
 */
class FacturaDocumentIsAvailableViewHelper extends AbstractViewHelper {

	/**
	 * @param NodeInterface $node
	 * @return boolean
	 * @throws Exception
	 */
	public function render(NodeInterface $node = NULL) {
		if($node === NULL){
			$node = $this->renderChildren();
		}
		if($node === NULL){
			throw new Exception('Node expected', 1410782487);
		}
		if (!$node->hasProperty('availableFrom')) {
			return TRUE;
		}
		if ($node->hasProperty('availableFrom') && $node->getProperty('availableFrom') < new \DateTime()) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
}
