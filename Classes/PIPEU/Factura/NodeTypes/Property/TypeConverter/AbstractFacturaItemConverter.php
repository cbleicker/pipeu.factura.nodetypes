<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\NodeTypes\Property\TypeConverter;

use TYPO3\Flow\Reflection\ObjectAccess;
use TYPO3\Flow\Property\PropertyMapper;
use TYPO3\Flow\Property\TypeConverter\AbstractTypeConverter;
use TYPO3\TYPO3CR\Domain\Model\NodeInterface;
use TYPO3\Flow\Annotations as Flow;

/**
 * Class AbstractFacturaItemConverter
 *
 * @package PIPEU\Factura\NodeTypes\Property\TypeConverter
 * @Flow\Scope("singleton")
 */
class AbstractFacturaItemConverter extends AbstractTypeConverter {

	/**
	 * @var PropertyMapper
	 * @Flow\Inject
	 */
	protected $propertyMapper;

	/**
	 * {@inheritDoc}
	 */
	protected $targetType = 'PIPEU\Factura\Domain\Abstracts\AbstractFacturaItem';

	/**
	 * {@inheritDoc}
	 */
	protected $sourceTypes = array('string');

	/**
	 * {@inheritDoc}
	 */
	protected $priority = 100;

	/**
	 * {@inheritDoc}
	 */
	public function canConvertFrom($source, $targetType) {
		/** @var NodeInterface $node */
		$node = $this->propertyMapper->convert($source, 'TYPO3\TYPO3CR\Domain\Model\NodeInterface');
		if ($node instanceof NodeInterface && $this->detectTargetTypeFromNode($node) !== NULL) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public function convertFrom($source, $targetType, array $convertedChildProperties = array(), \TYPO3\Flow\Property\PropertyMappingConfigurationInterface $configuration = NULL) {
		/** @var NodeInterface $node */
		$node       = $this->propertyMapper->convert($source, 'TYPO3\TYPO3CR\Domain\Model\NodeInterface');
		$targetType = $this->detectTargetTypeFromNode($node);
		$source     = $node->getProperties();
		$item       = $this->propertyMapper->convert($source, $targetType, $configuration);
		ObjectAccess::setProperty($item, 'contextPath', $node->getContextPath(), TRUE);
		return $item;
	}

	/**
	 * @param NodeInterface $node
	 * @return string|NULL
	 */
	protected function detectTargetTypeFromNode(NodeInterface $node) {
		$nodeTypeName = $node->getNodeType()->getName();
		switch ($nodeTypeName) {
			case 'PIPEU.Factura.NodeTypes:PhysicalProduct':
				return 'PIPEU\Factura\Domain\Model\Documents\Items\PhysicalProduct';
				break;
		}
		return NULL;
	}
}
