<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\NodeTypes\Domain\Model;

use TYPO3\Party\Domain\Model\ElectronicAddress;
use PIPEU\Geo\Domain\Model\Abstracts\AbstractPostal;
use TYPO3\Party\Domain\Model\PersonName;
use TYPO3\Flow\Annotations as Flow;

/**
 * Class InvoiceDto
 *
 * @package PIPEU\Factura\NodeTypes\Domain\Model
 */
class InvoiceDto {

	/**
	 * @var PersonName
	 */
	protected $personName;

	/**
	 * @var AbstractPostal
	 */
	protected $postal;

	/**
	 * @var string
	 */
	protected $vat;

	/**
	 * @var ElectronicAddress
	 */
	protected $electronicAddress;

	/**
	 * @var string
	 */
	protected $company;

	/**
	 * @param AbstractPostal $postal
	 * @param PersonName $personName
	 * @param string $vat
	 * @param ElectronicAddress $electronicAddress
	 * @param string $company
	 */
	public function __construct(AbstractPostal $postal = NULL, PersonName $personName = NULL, $vat = NULL, ElectronicAddress $electronicAddress = NULL, $company = NULL) {
		$this->postal = $postal;
		$this->personName = $personName;
		$this->vat = $vat;
		$this->electronicAddress = $electronicAddress;
		$this->company = $company;
	}

	/**
	 * @return AbstractPostal
	 */
	public function getPostal() {
		return $this->postal;
	}

	/**
	 * @return PersonName
	 */
	public function getPersonName() {
		return $this->personName;
	}

	/**
	 * @return string
	 */
	public function getVat() {
		return $this->vat;
	}

	/**
	 * @return ElectronicAddress
	 */
	public function getElectronicAddress() {
		return $this->electronicAddress;
	}

	/**
	 * @return string
	 */
	public function getCompany() {
		return $this->company;
	}
}
